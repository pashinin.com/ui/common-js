# @pashinin.com/common-js

## PROD: add `common-js` in your project

Create .yarnrc file in the root of your project with following content:

```
"@pashinin.com:registry" "https://gitlab.com/api/v4/packages/npm/"
```

and add a dependency:

```
yarn add @pashinin.com/common-js
```

## DEV: add `common-js` in your project

```
# in common-js project:
yarn link --link-folder ../tmp

# in other projects:
yarn link --link-folder ../../../tmp common-js
yarn link --link-folder ../../../tmp @pashinin.com/common-js
```

Do not forget to build this lib with `yarn build` (to create a `dist`
folder).
