import api from './api';
import Captcha from './captcha';

// declare var captchas: any;
// window.captchas = [];
// Declare global constant
declare global {
  interface Window {
    my: any;
    // __ezconsent: any;
  }
}


// function sleep(ms: number) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }


/* Serialize form to JSON format
 *
 */




export default async function common() {
  window.my = {
    forms: {
      callbacks: {},
    },
  };

  // Give functionality to "div.captcha" elements
  Captcha.enable();

  // All forms
  Array.from(document.querySelectorAll("form")).map(form => {
    // Disable default form's submit on Enter key,
    //
    // This is because forms need to be validated first. This is done in
    // JS.
    form.onsubmit = mySubmitFunction;

    // For all text inputs inside a form
    Array.from(form.querySelectorAll("input[type=text]")).map(input => {
      // When user changes text input
      input.addEventListener('input', (e) => {
        const txt = (e.target as HTMLElement).closest('.text-field');

        // clear error text
        txt.querySelector('.error').textContent = '';
      });
    });
  });
}
