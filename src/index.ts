import api from './api';
import Captcha from './captcha';
import common from './common';

const DOMReady = function(callback: any) {
  document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
};

export {
  api,
  Captcha,
  DOMReady,
  common,
};
