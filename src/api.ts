export default {
  hostname: function(): string {
    return window.location.host.includes('pashinin.localhost') ? 'api.pashinin.localhost' : 'api.pashinin.com';
  },
  post: async function(url: string, data: any) {
    const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached

      // credentials (send cookies)
      //
      // Variants: include, same-origin, omit
      //
      // I work with with cross-origin requests so: 'include'
      credentials: 'include',

      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return await response.json(); // parses JSON response into native JavaScript objects
  }
}
